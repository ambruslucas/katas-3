
const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

function kata1() {
    let kata1numeros = [];
    for (let i = 1; i <= 25; i++) {
        kata1numeros.push(i);
    }
    return kata1numeros;
    }

function kata2() {
    let kata2numeros = [];
    for (let i = 25; i >= 1; i--) {
        kata2numeros.push(i);
    }
    return kata2numeros;
    }
    
function kata3() {
    let kata3numeros = [];
    for (let i = -1; i >= -25; i--) {
    kata3numeros.push(i);
    }
    return kata3numeros;
    }

function kata4() {
    let kata4numeros = [];
    for (let i = -25; i <= -1; i++) {
    kata4numeros.push(i);
    }
    return kata4numeros;
    }

function kata5() {
    let kata5numeros = [];
    for (let i = 25; i >= -25; i = i-2) {
    kata5numeros.push(i);
    }
    return kata5numeros;
    }

function kata6() {
    let kata6numeros = [];
    for (let i = 3; i <= 100; i = i+3) {
    kata6numeros.push(i);
    }
    return kata6numeros;
    }

function kata7() {
    let kata7numeros = [];
    for (let i = 7; i <= 100; i = i+7) {
    kata7numeros.push(i);
    }
    return kata7numeros;
    }

function kata8() {
    let kata8numeros = [];
    for (let i = 100; i >= 1; i--) {
        if (i % 3 === 0 || i % 7 === 0) {
            kata8numeros.push(i);
        } 

    }   return kata8numeros;
}

function kata9() {
    let kata9numeros = [];
    for (let i = 5; i <= 100; i++) {
        if (i % 2 ==! 0 && i % 5 === 0) {
            kata9numeros.push(i);
        }
    }   return kata9numeros;
}

function kata10() {
    return sampleArray;
}

function kata11() {
    let kata11numeros = [];
    for (let i = 0; i < sampleArray.length; i++) {
        if ( sampleArray[i] % 2 === 0 ) {
            kata11numeros.push(sampleArray[i]);
        }
    } return kata11numeros;
}

function kata12() {
    let kata12numeros = [];
    for (let i = 0; i < sampleArray.length; i++) {
        if ( sampleArray[i] % 2 ==! 0 ) {
            kata12numeros.push(sampleArray[i]);
        }
    } return kata12numeros;
}

function kata13() {
    let kata13numeros = [];
    for (let i = 0; i < sampleArray.length; i++) {
        if ( sampleArray[i] % 8 === 0 ) {
            kata13numeros.push(sampleArray[i]);
        }
    } return kata13numeros;
}

function kata14() {
    let kata14numeros = [];
    for (let i = 0; i < sampleArray.length; i++) {
        
        kata14numeros.push(sampleArray[i] ** 2);
        
    } return (kata14numeros);
}

function kata15() {
    let kata15numeros = 0;
    for (let i = 1; i <= 20; i++) {
        kata15numeros = kata15numeros + i
    } return(kata15numeros);
}

let main = document.getElementById('main')
let kata01 = document.createElement('div')
let tk1 = document.createElement('h2')
main.appendChild(kata01)
kata01.appendChild(tk1)
tk1.innerHTML = 'Kata1'
kata01.innerHTML += kata1()

let kata02 = document.createElement('div')
let tk2 = document.createElement('h2')
main.appendChild(kata02)
kata02.appendChild(tk2)
tk2.innerHTML = 'Kata2'
kata02.innerHTML += kata2()

let kata03 = document.createElement('div')
let tk3 = document.createElement('h2')
main.appendChild(kata03)
kata03.appendChild(tk3)
tk3.innerHTML = 'Kata3'
kata03.innerHTML += kata3()

let kata04 = document.createElement('div')
let tk4 = document.createElement('h2')
main.appendChild(kata04)
kata04.appendChild(tk4)
tk4.innerHTML = 'Kata4'
kata04.innerHTML += kata4()

let kata05 = document.createElement('div')
let tk5 = document.createElement('h2')
main.appendChild(kata05)
kata05.appendChild(tk5)
tk5.innerHTML = 'Kata5'
kata05.innerHTML += kata5()

let kata06 = document.createElement('div')
let tk6 = document.createElement('h2')
main.appendChild(kata06)
kata06.appendChild(tk6)
tk6.innerHTML = 'Kata6'
kata06.innerHTML += kata6()

let kata07 = document.createElement('div')
let tk7 = document.createElement('h2')
main.appendChild(kata07)
kata07.appendChild(tk7)
tk7.innerHTML = 'Kata7'
kata07.innerHTML += kata7()

let kata08 = document.createElement('div')
let tk8 = document.createElement('h2')
main.appendChild(kata08)
kata08.appendChild(tk8)
tk8.innerHTML = 'Kata8'
kata08.innerHTML += kata8()

let kata09 = document.createElement('div')
let tk9 = document.createElement('h2')
main.appendChild(kata09)
kata09.appendChild(tk9)
tk9.innerHTML = 'Kata9'
kata09.innerHTML += kata9()

let kata010 = document.createElement('div')
let tk010 = document.createElement('h2')
main.appendChild(kata010)
kata010.appendChild(tk010)
tk010.innerHTML = 'Kata10'
kata010.innerHTML += kata10()

let kata011 = document.createElement('div')
let tk11 = document.createElement('h2')
main.appendChild(kata011)
kata011.appendChild(tk11)
tk11.innerHTML = 'Kata11'
kata011.innerHTML += kata11()

let kata012 = document.createElement('div')
let tk12 = document.createElement('h2')
main.appendChild(kata012)
kata012.appendChild(tk12)
tk12.innerHTML = 'Kata12'
kata012.innerHTML += kata12()

let kata013 = document.createElement('div')
let tk13 = document.createElement('h2')
main.appendChild(kata013)
kata013.appendChild(tk13)
tk13.innerHTML = 'Kata13'
kata013.innerHTML += kata13()

let kata014 = document.createElement('div')
let tk14 = document.createElement('h2')
main.appendChild(kata014)
kata014.appendChild(tk14)
tk14.innerHTML = 'Kata14'
kata014.innerHTML += kata14()

let kata015 = document.createElement('div')
let tk15 = document.createElement('h2')
main.appendChild(kata015)
kata015.appendChild(tk15)
tk15.innerHTML = 'Kata15'
kata015.innerHTML += kata15()

